import React from "react";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import makeStyles from "@material-ui/styles/makeStyles";

const StepCounter = ({ labels, activeStep }) => {
  const classes = useStyles();

  return (
    <div>
      <Stepper
        activeStep={activeStep}
        alternativeLabel
        className={classes.innerContainer}
      >
        {labels.map((label) => (
          <Step key={label} classes={{ horizontal: classes.size }}>
            <StepLabel
              classes={{
                active: classes.active,
                label: classes.label,
                completed: classes.active,
                alternativeLabel: classes.fontWeight,
              }}
            >
              {label}
            </StepLabel>
          </Step>
        ))}
      </Stepper>
    </div>
  );
};

const useStyles = makeStyles((theme) => ({
  innerContainer: {
    borderRadius: theme.shape.borderRadius,
    [theme.breakpoints.down("sm")]: {
      padding: theme.spacing(2, 0),
    },
  },

  label: {
    "&$active": {
      color: theme.palette.primary.main,
      fontWeight: "bold",
      [theme.breakpoints.down("sm")]: {
        fontSize: 10,
      },
    },
  },
  active: {},
  size: {
    "& svg": {
      fontSize: 30,
      [theme.breakpoints.down("sm")]: {
        fontSize: 24,
      },
    },
  },

  fontWeight: {
    fontWeight: "bold",
    [theme.breakpoints.down("sm")]: {
      fontSize: 10,
    },
  },
}));

export default StepCounter;
