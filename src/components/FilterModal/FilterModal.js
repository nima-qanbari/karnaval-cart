import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import Slide from "@material-ui/core/Slide";
import {
  AppBar,
  Button,
  DialogContent,
  IconButton,
  Toolbar,
  Typography,
} from "@material-ui/core";
import { Close } from "@material-ui/icons";

const useStyles = makeStyles(
  (theme) => ({
    appBar: {
      // position: "relative",
      marginBottom: theme.spacing(2),
    },

    toolbar: {
      display: "flex",
      justifyContent: "space-between",
      position: "relative",
    },

    sidebarContainer: {
      margin: theme.spacing(8.5, 1, 9),
      position: "relative",
    },

    btn: {
      position: "fixed",
      width: "100%",
      padding: theme.spacing(2, 0),
      bottom: 0,
      left: 0,
      right: 0,
      borderRadius: 0,
    },
  }),
  { flip: false }
);

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function FilterModal({ open, handleClose, children }) {
  const classes = useStyles();
  return (
    <Dialog
      fullScreen
      open={open}
      onClose={handleClose}
      TransitionComponent={Transition}
    >
      <AppBar color="default" className={classes.appBar}>
        <Toolbar className={classes.toolbar}>
          <Typography>فیلتر</Typography>
          <IconButton onClick={handleClose} color="inherit">
            <Close />
          </IconButton>
        </Toolbar>
      </AppBar>
      <DialogContent className={classes.sidebarContainer}>
        {children}
      </DialogContent>
      <Button
        variant="contained"
        color="primary"
        className={classes.btn}
        classes={{ root: classes.btn }}
      >
        اعمال تغییرات
      </Button>
    </Dialog>
  );
}

export default FilterModal;
