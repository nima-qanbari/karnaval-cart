import React, { useCallback, useEffect, useState } from "react";
import { makeStyles } from "@material-ui/styles";
import useEmblaCarousel from "embla-carousel-react";
import { Button, Typography } from "@material-ui/core";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";

const Carousel = ({ title, subtitle, data, width, cardHeight, card: Card }) => {
  const [canScroll, setCanScroll] = useState({
    next: false,
    prev: false,
  });

  const [emblaRef, emblaApi] = useEmblaCarousel({
    loop: false,
    direction: "rtl",
    dragFree: true,
    align: "start",
    containScroll: "trimSnaps",
  });

  const scrollPrev = useCallback(() => {
    if (emblaApi) emblaApi.scrollPrev();
  }, [emblaApi]);

  const scrollNext = useCallback(() => {
    if (emblaApi) emblaApi.scrollNext();
  }, [emblaApi]);

  useEffect(() => {
    const onSelect = () => {
      setCanScroll({
        next: emblaApi.canScrollNext(),
        prev: emblaApi.canScrollPrev(),
      });
    };

    if (emblaApi) {
      onSelect();
      emblaApi.on("select", onSelect);
    }
    return () => {
      if (emblaApi) {
        emblaApi.off("select", onSelect);
      }
    };
  }, [emblaApi]);

  const classes = useStyles();
  return (
    <div data-testid="carousel">
      <div className={classes.titleBtn}>
        <div>
          <Typography variant="h3" className={classes.title}>
            {title}
          </Typography>
          <Typography variant="h2" className={classes.subtitle}>
            {subtitle}
          </Typography>
        </div>
        <div className={classes.btnContainer}>
          <Button
            className={classes.btn}
            onClick={scrollPrev}
            data-testid="prevButton"
            disabled={!canScroll.prev}
          >
            <ChevronRightIcon />
          </Button>
          <Button
            className={classes.btn}
            onClick={scrollNext}
            data-testid="nextButton"
            disabled={!canScroll.next}
          >
            <ChevronLeftIcon />
          </Button>
        </div>
      </div>

      <div className={classes.embla} ref={emblaRef}>
        <div className={classes.embla_container}>
          {data.map((item) => {
            return (
              <div
                key={item.id}
                className={classes.embla_slide}
                style={{
                  flex: `0 0 ${
                    typeof width === "number" ? `${width}px` : width
                  }`,
                }}
              >
                <div className={classes.embla_slide_inner}>
                  <Card
                    height={cardHeight}
                    image={item.img}
                    title={item.label}
                    subtitle={item.subtitle}
                  />
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

const useStyles = makeStyles(
  (theme) => ({
    titleBtn: {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
    },

    btnContainer: {
      [theme.breakpoints.down("sm")]: {
        display: "none",
      },
    },

    title: {
      fontSize: 22,
      marginBottom: theme.spacing(1),
      fontWeight: "bold",
      lineHeight: 1.5,

      [theme.breakpoints.down("sm")]: {
        fontSize: 19,
      },
    },

    subtitle: {
      fontSize: 16,
      marginBottom: theme.spacing(2),
      color: theme.palette.text.secondary,

      [theme.breakpoints.down("sm")]: {
        fontSize: 14,
      },
    },

    embla: {
      overflow: "hidden",
    },
    embla_container: {
      display: "flex",
    },
    embla_slide: {
      "&:not(:last-child) $embla_slide_inner": {
        marginLeft: theme.spacing(3),

        [theme.breakpoints.down("sm")]: {
          marginLeft: theme.spacing(2),
        },
      },
    },
    embla_slide_inner: {},
    btn: {
      border: `2px solid ${theme.palette.divider}`,
      width: 32,
      height: 32,
      minWidth: 0,
      marginLeft: theme.spacing(0.3),
      backgroundColor: theme.palette.background.paper,

      "&:hover": {
        backgroundColor: theme.palette.background.paper,
      },
    },
  }),
  { flip: false }
);

export default Carousel;
