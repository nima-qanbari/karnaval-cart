import { render, screen } from "@testing-library/react";
import { ThemeProvider } from "@material-ui/core";
import { theme } from "../../../theme/default";
import ListTitle from "../ListTitle";

const setup = (props) => {
  render(
    <ThemeProvider theme={theme}>
      <ListTitle {...props} />
    </ThemeProvider>
  );
};

describe("ListTitle", () => {
  test("should render", () => {
    setup({ title: "اتوبوس", icon: "svg", breadcrumbs: ["کارناوال"] });
    const breadcrumbs = screen.getByTestId("  ");
    expect(breadcrumbs).toBeInTheDocument();
  });
});
