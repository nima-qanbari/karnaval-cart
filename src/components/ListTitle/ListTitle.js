import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import NavigateBeforeIcon from "@material-ui/icons/NavigateBefore";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import Link from "@material-ui/core/Link";

const ListTitle = ({ title, icon, breadcrumbs }) => {
  const classes = useStyles();
  return (
    <div className={classes.container} data-testid="breadcrumbs">
      <div className={classes.desktopBreadcrumbs}>
        <div className={classes.svgContainer}>
          <img src={icon} alt="busIcon" className={classes.svg} />
          <Typography variant="h5">{title}</Typography>
        </div>
        <Breadcrumbs
          separator={<NavigateBeforeIcon fontSize="small" />}
          aria-label="breadcrumb"
          className={classes.breadcrumbs}
        >
          {breadcrumbs.map((item, index) => (
            <Link
              href="/#"
              key={index}
              color="textSecondary"
              className={classes.link}
            >
              {item}
            </Link>
          ))}
        </Breadcrumbs>
      </div>
      <Paper className={classes.paper}>
        <div className={classes.svgContainer}>
          <img src={icon} alt="busIcon" className={classes.svg} />
          <Typography variant="h5">{title}</Typography>
        </div>
        <Breadcrumbs
          separator={<NavigateBeforeIcon fontSize="small" />}
          aria-label="breadcrumb"
          className={classes.breadcrumbs}
        >
          {breadcrumbs.map((item, index) => (
            <Link
              href="/#"
              key={index}
              color="textSecondary"
              className={classes.link}
            >
              {item}
            </Link>
          ))}
        </Breadcrumbs>
        <Grid container alignItems="center" className={classes.innerContainer}>
          <Grid item className={classes.time}>
            <ChevronRightIcon color="action" />
            <span> روز قبل</span>
          </Grid>
          <Grid item xs className={classes.date}>
            یکشنبه ۱۲ تیر ۱۴۰۱
          </Grid>
          <Grid item className={classes.time}>
            <span> روز بعد</span>
            <ChevronLeftIcon color="action" />
          </Grid>
        </Grid>
      </Paper>
    </div>
  );
};

const useStyles = makeStyles(
  (theme) => ({
    paper: {
      display: "none",
      padding: theme.spacing(1.5),
      [theme.breakpoints.down("sm")]: {
        display: "block",
      },
    },

    container: {
      marginTop: theme.spacing(),
    },

    desktopBreadcrumbs: {
      [theme.breakpoints.down("sm")]: {
        display: "none",
      },
    },

    breadcrumbs: {
      marginTop: theme.spacing(),
    },

    svgContainer: {
      display: "flex",
      alignItems: "center",

      "& h5": {
        fontSize: 22,

        [theme.breakpoints.down("sm")]: {
          fontSize: 18,
        },
      },
    },

    link: {
      fontSize: 13,

      [theme.breakpoints.down("sm")]: {
        fontSize: 11,
      },
    },

    svg: {
      width: 28,
      margin: theme.spacing(0, 0, 0.5, 0.5),
    },

    innerContainer: {
      borderTop: `1px solid ${theme.palette.divider}`,
      marginTop: theme.spacing(),
      paddingTop: theme.spacing(),
    },

    time: {
      display: "flex",
      alignItems: "center",
      padding: theme.spacing(0, 1),

      "& span": {
        fontSize: 11,
        color: theme.palette.text.secondary,
      },
    },

    date: {
      textAlign: "center",
      borderRight: `2px solid ${theme.palette.divider}`,
      borderLeft: `2px solid ${theme.palette.divider}`,
      fontSize: 12,
      fontWeight: "bold",
    },
  }),
  { flip: false }
);

export default ListTitle;
