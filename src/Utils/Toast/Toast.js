import React, { useState } from "react";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import { makeStyles } from "@material-ui/styles";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const Toast = ({ open, handleClose, valid, message }) => {
  const classes = useStyles();
  return (
    <Snackbar
      classes={{ root: classes.snackbar }}
      open={open}
      autoHideDuration={valid ? 5000 : 3000}
      onClose={handleClose}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
    >
      <Alert
        onClose={handleClose}
        severity={valid ? "error" : "success"}
        classes={{ root: classes.alert }}
      >
        {message}
      </Alert>
    </Snackbar>
  );
};

const useStyles = makeStyles(
  (theme) => ({
    snackbar: {
      [theme.breakpoints.down(600)]: {
        left: "0",
        right: "0",
        width: "100%",
        padding: theme.spacing()
      },
    },

    alert: {
      width: "100%",
    },
  }),
  { flip: false }
);

export default Toast;
