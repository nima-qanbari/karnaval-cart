import {useEffect, useRef} from 'react'
import StickySidebar from "../../Utils/StickySidebar/StickySidebar";

export const useStickySidebar = (options) => {
  const ref = useRef(null);
 useEffect(() => {
    const sticky = new StickySidebar(ref.current, options);
    return () => {
        sticky.destroy()
    }
 }, [])

 return ref
}

