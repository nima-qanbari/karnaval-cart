import React, { useState } from "react";

import "./App.css";
import "./cssStyles/table.css";

import { makeStyles } from "@material-ui/styles";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Landing from "./pages/Landing";
import TimerIcon from "@material-ui/icons/Timer";
import ArrowDownwardIcon from "@material-ui/icons/ArrowDownward";
import ArrowUpwardIcon from "@material-ui/icons/ArrowUpward";
import Tickets from "./pages/Tickets";

import DataEntry from "./pages/DataEntry";
import CheckoutPage from "./pages/Checkout";

const useStyles = makeStyles(
  (theme) => {
    return {
      max: {
        maxWidth: "1200px",
        margin: "0 auto",
        marginTop: theme.spacing(8),
        padding: theme.spacing(0, 2.5),

        [theme.breakpoints.down("sm")]: {
          padding: theme.spacing(0, 2.5),
        },
      },
    };
  },
  { flip: false }
);
const App = () => {
  const [visible, setVisible] = useState(100);
  const [sidebarChecked, setSidebarChecked] = useState([]);
  const [loading, setLoading] = useState(false);
  const [sortItem, setSortItem] = useState(null);

  const classes = useStyles();

  const sidebarCheckboxOnchange = (e) => {
    const value = e.target.value;

    const checkedIndex = sidebarChecked.findIndex((id) => id === value);

    if (checkedIndex > -1) {
      //remove item to array
      const result = [
        ...sidebarChecked.slice(0, checkedIndex),
        ...sidebarChecked.slice(checkedIndex + 1),
      ];
      setSidebarChecked(result);
    } else {
      //add item to array
      const result = [...sidebarChecked, value];
      setSidebarChecked(result);
    }
  };

  const moreOnclick = () => {
    setLoading(true);
    setTimeout(() => {
      setVisible((prevState) => prevState + 1);
      setLoading(false);
    }, 1000);
  };

  const sortHandler = (id) => {
    setSortItem(sortItem === id ? null : id);
  };

  const isSidebarChecked = (id) => {
    return sidebarChecked.includes(id);
  };
  const sortItems = [
    { label: "از صبح به شب", value: 1, icon: TimerIcon },
    { label: "از شب به صبح", value: 2, icon: TimerIcon },
    { label: "ارزان ترین", value: 3, icon: ArrowDownwardIcon },
    { label: "گران ترین", value: 4, icon: ArrowUpwardIcon },
  ];

  const originalData = [
    {
      title: "شرکت تی بی تی- تعاونى شماره 15 پايانه جنوب",
      subtitle: "VIPتخت شو+پذیرائی +ماسک",
      arrival: new Date(),
      departure: new Date(),
      origin: "تهران",
      destination: "مشهد",
      originTerminal: "تهران جنوب",
      destinationTerminal: "مشهد",
      price: 98000,
      remain: 29,
      vehicle: "اسکانیا ۳۱ نفره کلاسیک",
    },
    {
      title: "شرکت تی بی تی- تعاونى شماره 15 پايانه جنوب",
      subtitle: "VIPتخت شو+پذیرائی +ماسک",
      arrival: new Date(),
      departure: new Date(),
      origin: "تهران",
      destination: "مشهد",
      originTerminal: "تهران جنوب",
      destinationTerminal: "مشهد",
      price: 98000,
      remain: 29,
      vehicle: "اسکانیا ۳۱ نفره کلاسیک",
    },
    {
      title: "شرکت تی بی تی- تعاونى شماره 15 پايانه جنوب",
      subtitle: "VIPتخت شو+پذیرائی +ماسک",
      arrival: new Date(),
      departure: new Date(),
      origin: "تهران",
      destination: "مشهد",
      originTerminal: "تهران جنوب",
      destinationTerminal: "مشهد",
      price: 98000,
      remain: 29,
      vehicle: "اسکانیا ۳۱ نفره کلاسیک",
    },
    {
      title: "شرکت تی بی تی- تعاونى شماره 15 پايانه جنوب",
      subtitle: "VIPتخت شو+پذیرائی +ماسک",
      arrival: new Date(),
      departure: new Date(),
      origin: "تهران",
      destination: "مشهد",
      originTerminal: "تهران جنوب",
      destinationTerminal: "مشهد",
      price: 98000,
      remain: 29,
      vehicle: "اسکانیا ۳۱ نفره کلاسیک",
    },
    {
      title: "شرکت تی بی تی- تعاونى شماره 15 پايانه جنوب",
      subtitle: "VIPتخت شو+پذیرائی +ماسک",
      arrival: new Date(),
      departure: new Date(),
      origin: "تهران",
      destination: "مشهد",
      originTerminal: "تهران جنوب",
      destinationTerminal: "مشهد",
      price: 98000,
      remain: 29,
      vehicle: "اسکانیا ۳۱ نفره کلاسیک",
    },
    {
      title: "شرکت تی بی تی- تعاونى شماره 15 پايانه جنوب",
      subtitle: "VIPتخت شو+پذیرائی +ماسک",
      arrival: new Date(),
      departure: new Date(),
      origin: "تهران",
      destination: "مشهد",
      originTerminal: "تهران جنوب",
      destinationTerminal: "مشهد",
      price: 98000,
      remain: 29,
      vehicle: "اسکانیا ۳۱ نفره کلاسیک",
    },
    {
      title: "شرکت تی بی تی- تعاونى شماره 15 پايانه جنوب",
      subtitle: "VIPتخت شو+پذیرائی +ماسک",
      arrival: new Date(),
      departure: new Date(),
      origin: "تهران",
      destination: "مشهد",
      originTerminal: "تهران جنوب",
      destinationTerminal: "مشهد",
      price: 98000,
      remain: 29,
      vehicle: "اسکانیا ۳۱ نفره کلاسیک",
    },
    {
      title: "شرکت تی بی تی- تعاونى شماره 15 پايانه جنوب",
      subtitle: "VIPتخت شو+پذیرائی +ماسک",
      arrival: new Date(),
      departure: new Date(),
      origin: "تهران",
      destination: "مشهد",
      originTerminal: "تهران جنوب",
      destinationTerminal: "مشهد",
      price: 98000,
      remain: 29,
      vehicle: "اسکانیا ۳۱ نفره کلاسیک",
    },
    {
      title: "شرکت تی بی تی- تعاونى شماره 15 پايانه جنوب",
      subtitle: "VIPتخت شو+پذیرائی +ماسک",
      arrival: new Date(),
      departure: new Date(),
      origin: "تهران",
      destination: "مشهد",
      originTerminal: "تهران جنوب",
      destinationTerminal: "مشهد",
      price: 98000,
      remain: 29,
      vehicle: "اسکانیا ۳۱ نفره کلاسیک",
    },
    {
      title: "شرکت تی بی تی- تعاونى شماره 15 پايانه جنوب",
      subtitle: "VIPتخت شو+پذیرائی +ماسک",
      arrival: new Date(),
      departure: new Date(),
      origin: "تهران",
      destination: "مشهد",
      originTerminal: "تهران جنوب",
      destinationTerminal: "مشهد",
      price: 98000,
      remain: 29,
      vehicle: "اسکانیا ۳۱ نفره کلاسیک",
    },
    {
      title: "شرکت تی بی تی- تعاونى شماره 15 پايانه جنوب",
      subtitle: "VIPتخت شو+پذیرائی +ماسک",
      arrival: new Date(),
      departure: new Date(),
      origin: "تهران",
      destination: "مشهد",
      originTerminal: "تهران جنوب",
      destinationTerminal: "مشهد",
      price: 98000,
      remain: 29,
      vehicle: "اسکانیا ۳۱ نفره کلاسیک",
    },
    {
      title: "شرکت تی بی تی- تعاونى شماره 15 پايانه جنوب",
      subtitle: "VIPتخت شو+پذیرائی +ماسک",
      arrival: new Date(),
      departure: new Date(),
      origin: "تهران",
      destination: "مشهد",
      originTerminal: "تهران جنوب",
      destinationTerminal: "مشهد",
      price: 98000,
      remain: 29,
      vehicle: "اسکانیا ۳۱ نفره کلاسیک",
    },
    {
      title: "شرکت تی بی تی- تعاونى شماره 15 پايانه جنوب",
      subtitle: "VIPتخت شو+پذیرائی +ماسک",
      arrival: new Date(),
      departure: new Date(),
      origin: "تهران",
      destination: "مشهد",
      originTerminal: "تهران جنوب",
      destinationTerminal: "مشهد",
      price: 98000,
      remain: 29,
      vehicle: "اسکانیا ۳۱ نفره کلاسیک",
    },
    {
      title: "شرکت تی بی تی- تعاونى شماره 15 پايانه جنوب",
      subtitle: "VIPتخت شو+پذیرائی +ماسک",
      arrival: new Date(),
      departure: new Date(),
      origin: "تهران",
      destination: "مشهد",
      originTerminal: "تهران جنوب",
      destinationTerminal: "مشهد",
      price: 98000,
      remain: 29,
      vehicle: "اسکانیا ۳۱ نفره کلاسیک",
    },
  ];

  const data = originalData.slice(0, visible);

  return (
    <div className={classes.max}>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Landing />} />
          <Route
            path="/tickets"
            element={
              <Tickets
                hasMore={visible < originalData.length}
                moreOnclick={moreOnclick}
                loading={loading}
                data={data}
                sort={sortItems}
                selectedSortItem={sortItem}
                sortHandler={sortHandler}
                onChangeCheckbox={sidebarCheckboxOnchange}
                isChecked={isSidebarChecked}
                sidebar={[
                  {
                    label: "ترمینال مبدا",
                    items: [
                      {
                        id: "11",
                        label: "ترمینال بیهقی",
                        count: 4,
                      },
                      {
                        id: "12",
                        label: "ترمینال جنوب",
                        count: 4,
                      },
                      {
                        id: "13",
                        label: "ترمینال غرب",
                        count: 4,
                      },
                      {
                        id: "14",
                        label: "ترمینال شرق",
                        count: 4,
                      },
                    ],
                  },
                  {
                    label: "ترمینال مبدا",
                    items: [
                      {
                        id: "11",
                        label: "ترمینال بیهقی",
                        count: 4,
                      },
                      {
                        id: "12",
                        label: "ترمینال جنوب",
                        count: 4,
                      },
                      {
                        id: "13",
                        label: "ترمینال غرب",
                        count: 4,
                      },
                      {
                        id: "14",
                        label: "ترمینال شرق",
                        count: 4,
                      },
                    ],
                  },
                  {
                    label: "ترمینال مبدا",
                    items: [
                      {
                        id: "11",
                        label: "ترمینال بیهقی",
                        count: 4,
                      },
                      {
                        id: "12",
                        label: "ترمینال جنوب",
                        count: 4,
                      },
                      {
                        id: "13",
                        label: "ترمینال غرب",
                        count: 4,
                      },
                      {
                        id: "14",
                        label: "ترمینال شرق",
                        count: 4,
                      },
                    ],
                  },

                  {
                    label: "ترمینال مقصد",
                    items: [
                      {
                        id: "21",
                        label: "ترمینال بیهقی",
                        count: 4,
                      },
                      {
                        id: "22",
                        label: "ترمینال جنوب",
                        count: 4,
                      },
                      {
                        id: "23",
                        label: "ترمینال غرب",
                        count: 4,
                      },
                      {
                        id: "24",
                        label: "ترمینال شرق",
                        count: 4,
                      },
                    ],
                  },
                ]}
              />
            }
          />
          <Route path="/dataEntry" element={<DataEntry />} />
          <Route path="/checkout" element={<CheckoutPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
};

export default App;
