import React, { useState } from "react";
import Grid from "@material-ui/core/Grid";
import { Form } from "react-final-form";
import { FieldArray } from "react-final-form-arrays";
import arrayMutators from "final-form-arrays";
import SupervisorForm from "../components/SupervisorForm/SupervisorForm";
import PassengerDetail from "../components/PassengerDetail/PassengerDetail";
import StepCounter from "../components/StepCounter/StepCounter";
import { useTheme } from "@material-ui/core/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import Toast from "../Utils/Toast/Toast";

const busStepsDesktop = [
  "انتخاب بلیط",
  "مشخصات مسافران",
  "تایید و پرداخت",
  "صدور بلیط",
];
const busStepsMobile = ["مسافران", "تایید و پرداخت", "صدور بلیط"];

const DataEntry = () => {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const [openToast, setOpenToast] = useState(false);

  const onSubmit = (data) => {
    console.log("ddfdsfd");
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, 2000);
    });
  };

  const closeToastHandler = () => {
    setOpenToast(false);
  };

  const openToastHandler = () => {
    setOpenToast(true);
  };
  return (
    <div>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <StepCounter
            labels={isMobile ? busStepsMobile : busStepsDesktop}
            activeStep={isMobile ? 0 : 1}
          />
        </Grid>
        <Grid item xs={12}>
          <Form
            initialValues={{ mobile: "09911339988", passengers: [{}] }}
            onSubmit={onSubmit}
            mutators={{
              ...arrayMutators,
            }}
            render={({
              handleSubmit,
              form: {
                mutators: { push, pop },
              },
              invalid,
            }) => {
              return (
                <form
                  onSubmit={(event) => {
                    event.preventDefault();
                    if (invalid) {
                      openToastHandler();
                      window.scroll(0, 0);
                    }
                    handleSubmit(event);
                  }}
                >
                  <FieldArray name="passengers">
                    {({ fields }) =>
                      fields.map((name, index) => {
                        return <PassengerDetail name={name} key={index} />;
                      })
                    }
                  </FieldArray>
                  <Toast
                    open={openToast}
                    handleClose={closeToastHandler}
                    valid={invalid}
                    message="مشخصات مسافران را کامل و صحیح وارد کنید"
                  />
                  <SupervisorForm />
                </form>
              );
            }}
          />
        </Grid>
      </Grid>
    </div>
  );
};

export default DataEntry;
