import React, { useState } from "react";
import makeStyles from "@material-ui/styles/makeStyles";

import Grid from "@material-ui/core/Grid";
import Sidebar from "../components/Sidebar/Sidebar";
import FilterModal from "../components/FilterModal/FilterModal";
import SortModal from "../components/SortModal/SortModal";
import Toolbar from "../components/Toolbar/Toolbar";
import Button from "@material-ui/core/Button";
import SortDesktop from "../components/SortDesktop/SortDesktop";
import TicketCard from "../components/TicketCard/TicketCard";
import Typography from "@material-ui/core/Typography";
import FilterListIcon from "@material-ui/icons/FilterList";
import ImportExportIcon from "@material-ui/icons/ImportExport";

import { useTheme } from "@material-ui/core/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import ListTitle from "../components/ListTitle/ListTitle";

//bus SVG
import busSvg from "../SVG/Bus.svg";

const breadcrumbs = ["کارناوال", "بلیط اتوبوس", "بلیط اتوبوس تهران به شیراز"];

const Tickets = ({
  data,
  sidebar,
  sort,
  selectedSortItem,
  sortHandler,
  moreOnclick,
  hasMore,
  loading,
  onChangeCheckbox,
  isChecked,
}) => {
  const [openFilter, setOpenFilter] = useState(false);
  const [openSort, setOpenSort] = useState(false);
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const classes = useStyles();

  const openFilterHandler = () => {
    setOpenFilter(true);
  };

  const closeFilterHandler = () => {
    setOpenFilter(false);
  };

  const openSortHandler = () => {
    setOpenSort(true);
  };

  const closeSortHandler = () => {
    setOpenSort(false);
  };

  const sidebarJSX = (
    <Sidebar data={sidebar} onChange={onChangeCheckbox} isChecked={isChecked} />
  );

  return (
    <div>
      <Grid container spacing={2}>
        <FilterModal handleClose={closeFilterHandler} open={openFilter}>
          {sidebarJSX}
        </FilterModal>
        <SortModal
          handleClose={closeSortHandler}
          open={openSort}
          sort={sort}
          sortHandler={(id) => {
            sortHandler(id);
            closeSortHandler();
          }}
          selectedSortItem={selectedSortItem}
        />

        <Grid item xs={12}>
          <Toolbar useMinimize={isMobile} />
        </Grid>
        <Grid item xs={12}>
          <ListTitle
            breadcrumbs={breadcrumbs}
            title="بلیط اتوبوس تهران به شیراز"
            icon={busSvg}
          />
        </Grid>
        <Grid item xs={12}>
          <div className={classes.mobileFilter}>
            <Typography
              className={classes.alignment}
              onClick={openFilterHandler}
            >
              <FilterListIcon />
              فیلتر
            </Typography>
            <Typography className={classes.alignment} onClick={openSortHandler}>
              <ImportExportIcon />
              ترتیب
            </Typography>
          </div>
        </Grid>
        <Grid item md={3} className={classes.none}>
          {sidebarJSX}
        </Grid>
        <Grid item xs={12} md={9}>
          <SortDesktop
            sort={sort}
            sortHandler={sortHandler}
            selectedSortItem={selectedSortItem}
          />
          <div className={classes.items}>
            {data.map((item, index) => {
              return (
                <TicketCard
                  key={index}
                  logo={require("../image/download.png")}
                  title={item.title}
                  subtitle={item.subtitle}
                  departure={item.departure}
                  arrival={item.arrival}
                  origin={item.origin}
                  destination={item.destination}
                  originTerminal={item.originTerminal}
                  destinationTerminal={item.destinationTerminal}
                  vehicle={item.vehicle}
                  price={item.price}
                  remain={item.remain}
                />
              );
            })}
          </div>
          <div className={classes.hasMoreBtn}>
            {hasMore ? (
              <Button
                variant="contained"
                color="primary"
                onClick={moreOnclick}
                disabled={loading}
              >
                بیشتر
              </Button>
            ) : null}
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

const useStyles = makeStyles(
  (theme) => ({
    paddingContainer: {
      padding: theme.spacing(),
    },
    none: {
      [theme.breakpoints.down("sm")]: {
        display: "none",
      },
    },

    alignment: {
      display: "flex",
      alignItems: "center",
      fontSize: 13,
      fontWeight: 500,
      cursor: "pointer",

      "& svg": {
        marginLeft: theme.spacing(0.7),
        color: theme.palette.text.secondary,
        fontSize: 20,
        fontWeight: "bold",
      },
    },

    mobileFilter: {
      display: "none",
      borderRadius: theme.shape.borderRadius,
      background: theme.palette.background.paper,

      "& p": {
        display: "flex",
        justifyContent: "center",
        width: "48%",
        padding: theme.spacing(2, 0),
      },
      [theme.breakpoints.down("sm")]: {
        display: "flex",
        justifyContent: "space-around",
      },
    },

    items: {
      "& > :not(:last-child)": {
        marginBottom: theme.spacing(2),
      },
    },

    hasMoreBtn: {
      textAlign: "center",
      margin: theme.spacing(2, 0, 0),

      "& > button": {
        fontSize: 20,
        fontWeight: "bold",
        padding: theme.spacing(0.5, 6),
      },
    },
  }),
  { flip: false }
);

export default Tickets;
