import React from "react";
import Grid from "@material-ui/core/Grid";
import Checkout from "../components/Checkout/Checkout";
import StepCounter from "../components/StepCounter/StepCounter";
import { useTheme } from "@material-ui/core/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";

const busStepsDesktop = [
  "انتخاب بلیط",
  "مشخصات مسافران",
  "تایید و پرداخت",
  "صدور بلیط",
];
const busStepsMobile = ["مسافران", "تایید و پرداخت", "صدور بلیط"];
const CheckoutPage = () => {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  return (
    <div>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <StepCounter
            labels={isMobile ? busStepsMobile : busStepsDesktop}
            activeStep={isMobile ? 1 : 2}
          />
        </Grid>
        <Grid item xs={12}>
          <Checkout
            data={[
              { label: "نام تعاونی", value: "شرکت زاگرس" },
              { label: "مبدا", value: "مشهد" },
              { label: "مقصد", value: "تهران" },
              { label: "تاریخ حرکت", value: "یکشنبه ۱۲ تیر ۱۴۰۱" },
              { label: "ساعت حرکت", value: "۲۳:۳۰" },
            ]}
            passengers={[
              {
                name: "نیما",
                family: "قنبری",
                gender: "male",
                nationalCode: "3242047672",
                seatNumber: "8",
                price: 169000,
              },
              {
                name: "ییسب",
                family: "یسیسیسر",
                gender: "female",
                nationalCode: "3242047672",
                seatNumber: "9",
                price: 169000,
              },
            ]}
            cancellationRules={[
              "از زمان صدور تا ۹۰ دقیقه قبل از حرکت | استرداد آنلاین",
              "کمتر از یک ساعت قبل از حرکت تا بعد از حرکت | استرداد حضوری در پایانه مسافربری",
            ]}
            ticketInformation="اطلاعات اتوبوس"
            totalPrice={998000}
            passengersInformation="اطلاعات مسافران"
          />
        </Grid>
      </Grid>
    </div>
  );
};

export default CheckoutPage;
