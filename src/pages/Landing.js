import React from "react";
import Grid from "@material-ui/core/Grid";
import Carousel from "../components/Carousel/Carousel";
import FullCard from "../components/Card/FullCard";
import PaperCard from "../components/Card/PaperCard";
import Blog from "../components/Blog/Blog";
import Carousel2 from "../components/Carousel2/Carousel2";

const Landing = () => {
  return (
    <div style={{ maxWidth: 1100, margin: "auto" }}>
      <Grid container>
        <Grid item xs={12}>
          <Carousel
            card={PaperCard}
            title="شهرهای خنک ایران برای سفرهای تابستانی"
            subtitle="مقاصد تابستانی جذاب ایران برای سفر"
            width={"25%"}
            cardHeight={220}
            data={[
              {
                id: 1,
                img: require("../image/manzare.jpg"),
                label: "خراسان شمالی",
                subtitle: "مقاصد تابستانی جذاب",
              },
              {
                id: 2,
                img: require("../image/manzare.jpg"),
                label: "رشت",
                subtitle: "مقاصد تابستانی جذاب",
              },
              {
                id: 3,
                img: require("../image/manzare.jpg"),
                label: "یزد",
                subtitle: "مقاصد تابستانی جذاب",
              },
              {
                id: 4,
                img: require("../image/manzare.jpg"),
                label: "کیش",
                subtitle: "مقاصد تابستانی جذاب",
              },
              {
                id: 5,
                img: require("../image/manzare.jpg"),
                label: "قشم",
                subtitle: "مقاصد تابستانی جذاب",
              },
              {
                id: 6,
                img: require("../image/manzare.jpg"),
                label: "اصفهان",
                subtitle: "مقاصد تابستانی جذاب",
              },
              {
                id: 7,
                img: require("../image/manzare.jpg"),
                label: "اصفهان",
                subtitle: "مقاصد تابستانی جذاب",
              },
              {
                id: 8,
                img: require("../image/manzare.jpg"),
                label: "اصفهان",
                subtitle: "مقاصد تابستانی جذاب",
              },
            ]}
          />
        </Grid>
        <Grid item xs={12}>
          <Carousel
            card={FullCard}
            title="شهرهای خنک ایران برای سفرهای تابستانی"
            subtitle="مقاصد تابستانی جذاب ایران برای سفر"
            width={200}
            cardHeight={220}
            data={[
              {
                id: 1,
                img: require("../image/manzare.jpg"),
                label: "خراسان شمالی",
              },
              {
                id: 2,
                img: require("../image/manzare.jpg"),
                label: "رشت",
              },
              {
                id: 3,
                img: require("../image/manzare.jpg"),
                label: "یزد",
              },
              {
                id: 4,
                img: require("../image/manzare.jpg"),
                label: "کیش",
              },
              {
                id: 5,
                img: require("../image/manzare.jpg"),
                label: "قشم",
              },
              {
                id: 6,
                img: require("../image/manzare.jpg"),
                label: "اصفهان",
              },
            ]}
          />
        </Grid>
        <Grid item xs={12}>
          <Blog
            title="بلاگ"
            subtitle="تازه های کارناوال"
            data={[
              {
                label: "معرفی شهر های توریستی",
                subtitle: "معرفی شهر ماسال",
                img: require("../image/img.jpg"),
                md: 6,
              },
              {
                label: "معرفی شهر های توریستی",
                subtitle: "معرفی شهر ماسال",
                img: require("../image/img.jpg"),
                md: 6,
              },
              {
                label: "معرفی شهر های توریستی",
                subtitle: "معرفی شهر ماسال",
                img: require("../image/img.jpg"),
                md: 4,
              },
              {
                label: "معرفی شهر های توریستی",
                subtitle: "معرفی شهر ماسال",
                img: require("../image/img.jpg"),
                md: 4,
              },
              {
                label: "معرفی شهر های توریستی",
                subtitle: "معرفی شهر ماسال",
                img: require("../image/img.jpg"),
                md: 4,
              },
            ]}
          />
        </Grid>
      </Grid>
    </div>
  );
};

export default Landing;
